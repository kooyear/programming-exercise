//
//  NSDate+Extension.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.
//

import Foundation

extension NSDate {
    
    
    func getDateTimeWithFormat(format:String) -> String{
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter.stringFromDate(self)
        
    }
    
    //
    /**
    Get date in normal format "yyyyMMdd", e.g. 20150922
    
    - returns: date in format "yyyyMMdd"
    */
    func getDateWithNormalFormat()->String{
        
        return self.getDateTimeWithFormat("yyyyMMdd")
    }
    
    
    
}
