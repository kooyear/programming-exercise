//
//  ViewController.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, CLLocationManagerDelegate {
    
    
    var locationManager: CLLocationManager!
    var dataSourceForTableView:[PlaceModel]!
    
    var numberOfSectionsInTableView = 1
    
    var pubUtility = PublicUtility()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = getLocationManager()
        locationManager.startUpdatingLocation()
        
        dataSourceForTableView = []
        self.view.showHUDViewWithText("Loading ... ")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func refresh(sender: AnyObject) {
        
        self.view.hideHUDView()
        locationManager.startUpdatingLocation()
        self.view.showHUDViewWithText("Loading ... ")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return numberOfSectionsInTableView
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataSourceForTableView.count
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PlaceTableViewCell
        
        
        var cellData = dataSourceForTableView[indexPath.row]
        
        cell.nameLabel.text = cellData.name
        cell.addressLabel.text = cellData.getDistanceInFormat()+" "+cellData.address
        cell.callingView.alpha = 1
        
        // if there is't phone number info, hide the phone icon
        if cellData.phone == "null" {
            
            cell.callingView.alpha = 0
            
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 60
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.setSelected(false, animated: true)
        
        pubUtility.showCellClickOptions(dataSourceForTableView[indexPath.row])
        
    }
    
    /**
    Reload data for tableview with the newest data from server
    
    - parameter dataSource: the newest PlaceModel list
    */
    func reloadDataAfterDownLoad(dataSource:[PlaceModel]){
        
        dataSourceForTableView = dataSource
        self.view.hideHUDView()
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
        
    }
    
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        
        var location = locations.last as! CLLocation
        PublicUtility.currentLocation = location
        
        var latitude = location.coordinate.latitude.description
        var longitude = location.coordinate.longitude.description
        
        // after getting the newest location, update coffee shops info from server accoring to the user location
        ServiceUtility.fetchPlaceData(latitude, longitude: longitude) { (dataList, error) -> Void in
            if let dataList = dataList {
                self.reloadDataAfterDownLoad(dataList)
            } else {
                // failed
                self.view.hideHUDView()
                PublicUtility.showAlertAboutServiceError(error!.localizedDescription)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        
        self.view.hideHUDView()
        PublicUtility.showAlertAboutFailToGetLocation()
        
    }
    
    func getLocationManager() -> CLLocationManager {
        
        var locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 0.1
        locationManager.requestAlwaysAuthorization();
        locationManager.delegate = self
        
        return locationManager
    }
    
    
    
}

