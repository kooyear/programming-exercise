//
//  UIView+Extension.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.
//

import Foundation
extension UIView {
    
    /**
    Show progress hud on given view
    
    - parameter msg: message information need to show
    */
    func showHUDViewWithText(msg:String){
        
        var loading = MBProgressHUD.showHUDAddedTo(self, animated: true)
        loading.labelText = msg
        
    }
    
    /**
    Hide progress hud on given view
    */
    func hideHUDView() {
        
        MBProgressHUD.hideHUDForView(self, animated: true)
        
    }
    
    
}