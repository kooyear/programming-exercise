//
//  ServiceUtility.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.

//  This class is used to provide functions to communicate with server side and get services

import Foundation
import Alamofire
import SwiftyJSON

class ServiceUtility{
    
    static let clientId = "ACAO2JPKM1MXHQJCK45IIFKRFR2ZVL0QASMCBCG5NPJQWF2G"
    static let clientSecrer = "YZCKUYJ1WHUV2QICBXUBEILZI1DMPUIDP5SHV043O04FKBHL"
    static let query = "coffee shop"
    static var version = NSDate().getDateWithNormalFormat()
    
    
    /**
    Fetch data from foursquare. The searching keyword is "coffee shop"
    
    - parameter latitude:  The latitude of current location
    - parameter longitude: The longitude of current location
    - parameter callback:  Callback with fetched data. If error is not nil, then the fetching is failed.
    */
    
    class func fetchPlaceData(latitude:String,longitude:String,callback:([PlaceModel]?, NSError?)->Void) {
        var url = "https://api.foursquare.com/v2/venues/search"
        
        var params = [
            "client_id":clientId,
            "client_secret":clientSecrer,
            "v":version,
            "ll":"\(latitude),\(longitude)",
            "query":query
        ]
        
        Alamofire.request(.GET, url, parameters: params)
            .response { request, response, data, error in
                
                // if there is an error when connecting to server, show the error info to user via an alert view
                if error != nil {
                    
                    callback(nil, error)
                    
                }
                
                var json = JSON(data:data!)
                
                // transfer JSON format data to PlaceMode list as the datasource
                var newData = PlaceModel.getPlaceModelsViaJson(json)
                
                //update coffee shops nearby info
                callback(newData, nil)
        }
    }
    
    
    
}