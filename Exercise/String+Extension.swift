//
//  String+Extension.swift
//  Exercise
//
//  Created by GooD-YeaR on 22/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.
//

import Foundation
extension String {
    
    
    func doubleValue() -> Double{
        
        return (self as NSString).doubleValue
        
    }
    
}
