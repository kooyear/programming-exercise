//
//  PlaceModel.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.

//  This class is as a model to store place info

import Foundation
import SwiftyJSON


class PlaceModel{

    var name:String!
    var distance:Double!
    var address:String!
    var latitude:Double!
    var longitude:Double!
    var phone:String!

    /**
    Transfer JSON fomat data to PlaceModel list and sort the list by distance in ascending order
    
    - parameter json: JSON fomat data fetched from server
    
    - returns: PlaceModel list
    */
    class func getPlaceModelsViaJson(json:JSON) -> [PlaceModel] {
    
        var ls = [PlaceModel]()
        for j in json["response"]["venues"] {
        
        
            var placeModel = PlaceModel()
            
            placeModel.name = j.1["name"].description
            
            placeModel.distance = j.1["location"]["distance"].description.doubleValue()
            placeModel.address = assembleAddress(j.1["location"]["formattedAddress"])
            
            placeModel.latitude = j.1["location"]["lat"].description.doubleValue()
            placeModel.longitude = j.1["location"]["lng"].description.doubleValue()

            placeModel.phone = j.1["contact"]["phone"].description
            
            ls.append(placeModel)
        
        }
        
        ls.sort({ $0.distance < $1.distance })
        
    
    return ls
    
    }
    
    /**
    Transfer address list in JSON data to the whole distance string
    
    - parameter json: JSON fomat data fetched from server
    
    - returns: the whole distance string
    */
    class func assembleAddress(json:JSON) -> String{
        
        var addr = ""
        
        for j in json {
        
        
            addr += j.1.description + " "
        
        
        }
    
    return addr
    
    }
    
    /**
    Transfer distance info in JSON data to readable format
    
    - returns: readable distance info
    */
    func getDistanceInFormat() -> String {
    
        if self.distance < 1 {
            return "(lest than 1m)"
        }
        
        if self.distance >= 1000 {
            
                return "(\(self.distance/1000)km)"
            
            }
            
        return "(\(self.distance)m)"
            
        

    }

    /**
    Get coordinate info of this place
    
    - returns: coordinate info in "latitude,longitude" format
    */
    func getLocation() -> String {
    
        return "\(self.latitude),\(self.longitude)"
    
    }
}