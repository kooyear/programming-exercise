//
//  PublicUtility.swift
//  Exercise
//
//  Created by GooD-YeaR on 21/09/2015.
//  Copyright (c) 2015 YeGU. All rights reserved.

//  This class is used to provide some public functions for normal useage, such as show alert view. And it also is used
//  for storing some public variables, such as user current location

import Foundation
import CoreLocation

class PublicUtility:NSObject,UIAlertViewDelegate{
    
    var selectedModel:PlaceModel!
    static var currentLocation:CLLocation!
    
    class func showAlertAboutFailToGetLocation(){
        
        var alertView = UIAlertView()
        alertView.message = "Fail to get your current location."
        alertView.title = "Information"
        alertView.addButtonWithTitle("OK")
        alertView.show()
        
    }
    
    class func showAlertAboutServiceError(errorMaessage:String){
        
        var alertView = UIAlertView()
        alertView.message = errorMaessage
        alertView.title = "Information"
        alertView.addButtonWithTitle("OK")
        alertView.show()
        
    }
    
    func showCellClickOptions(selectedModel:PlaceModel){
        
        var alertView = UIAlertView()
        alertView.message = "Please select one action"
        alertView.title = "Information"
        
        if selectedModel.phone != "null" {
            alertView.addButtonWithTitle("Call")
        }
        alertView.addButtonWithTitle("Show in Map")
        alertView.addButtonWithTitle("Cancel")
        alertView.tag = 0
        alertView.delegate = self
        
        self.selectedModel = selectedModel
        
        alertView.show()
        
    }
    
    func alertView(view: UIAlertView, clickedButtonAtIndex buttonIndex: Int){
        
        if view.tag == 0 {
            
            if  view.buttonTitleAtIndex(buttonIndex) == "Call" {
                
                //If user touch the "Call" option, then turn to dial the number step
                UIApplication.sharedApplication().openURL(NSURL(string: "tel://\(self.selectedModel.phone)")!)
                
                
            }else if  view.buttonTitleAtIndex(buttonIndex) == "Show in Map" {
                
                //If user touch the "Show in Map" option, then show the coffee shop in Map App
                UIApplication.sharedApplication().openURL(NSURL(string: "http://maps.apple.com/?saddr=\(PublicUtility.currentLocation.coordinate.latitude),\(PublicUtility.currentLocation.coordinate.longitude)&daddr=\(self.selectedModel.getLocation())")!)
                
            }
            
        }
    }
    
    
    
}