## Running Environment Requirements

- iOS 8.4+
- Xcode 6.4

## Installation

1. Since I use the CoocaPods to do dependency management, you need to install CocoaPods firstly.
2. open project with “Exercise.xcwokspace” file with Xcode 6.4
